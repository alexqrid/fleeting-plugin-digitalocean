package main

import (
	"flag"
	"fmt"
	digitalocean "gitlab.com/alexqrid/fleeting-plugin-digitalocean"
	"gitlab.com/gitlab-org/fleeting/fleeting/plugin"
	"os"
)

var (
	showVersion = flag.Bool("version", false, "Show version information and exit")
)

func main() {
	flag.Parse()
	if *showVersion {
		fmt.Println(digitalocean.Version.Full())
		os.Exit(0)
	}

	plugin.Serve(&digitalocean.InstanceGroup{})
}
