package doclient

import (
	"context"
	"fmt"
	"github.com/digitalocean/godo"
	"golang.org/x/oauth2"
	"log"
	"net/http"
	"strconv"
	"time"
)

type TokenSource struct {
	AccessToken string
}

func (t *TokenSource) Token() (*oauth2.Token, error) {
	token := &oauth2.Token{
		AccessToken: t.AccessToken,
	}
	return token, nil
}

// NewClient initializes new DigitalOcean client
func NewClient(token string) (*godo.Client, error) {
	tokenSource := &TokenSource{
		AccessToken: token,
	}
	oauthClient := oauth2.NewClient(context.Background(), tokenSource)

	client := godo.NewClient(oauthClient)
	_, resp, err := client.Account.Get(context.Background())
	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("failed to obtain account info from DO: code=%d, err=%s", resp.StatusCode, err)
	}
	return client, nil
}

// CreateOrGetSSHKey creates ssh key in DigitalOcean account,
// if the key already exists in DO, retrieves its ID and
// returns it
func CreateOrGetSSHKey(PublicKey string, client *godo.Client) (int, error) {
	ctx := context.Background()
	key, resp, err := client.Keys.Create(ctx,
		&godo.KeyCreateRequest{
			Name:      fmt.Sprintf("ci-runner-key-%d", time.Now().Unix()),
			PublicKey: PublicKey,
		})

	if resp.StatusCode != http.StatusCreated {
		opt := &godo.ListOptions{
			PerPage: 100,
		}
		// paginate over list of keys
		for {
			keys, resp, err := client.Keys.List(ctx, opt)
			if err != nil {
				return 0, nil
			}
			for _, key := range keys {
				if key.PublicKey == PublicKey {
					return key.ID, nil
				}
			}

			if resp.Links == nil || resp.Links.IsLastPage() {
				break
			}
			page, err := resp.Links.CurrentPage()
			if err != nil {
				return -1, fmt.Errorf("key pagination error %s", err)
			}
			opt.Page = page + 1
		}
	}
	// Key wasn't found among the list of keys and there is an error in the API
	if err != nil {
		return 0, fmt.Errorf("key wasn't found and couldn't be created")
	}

	return key.ID, nil
}

func ListDroplets(tag string, client *godo.Client) ([]godo.Droplet, error) {
	var result []godo.Droplet
	opt := &godo.ListOptions{
		PerPage: 100,
	}

	for {
		droplets, resp, err := client.Droplets.ListByTag(context.Background(), tag, opt)
		if err != nil {
			return nil, fmt.Errorf("error occured while listing droplets: code=%d err=%s", resp.StatusCode, err)
		}
		result = append(result, droplets...)
		if resp.Links == nil || resp.Links.IsLastPage() {
			break
		}
		page, err := resp.Links.CurrentPage()
		if err != nil {
			return nil, fmt.Errorf("error occured while getting current api page of droplets: err=%s", err)
		}
		opt.Page = page + 1
	}
	return result, nil
}

func CreateDroplet(client *godo.Client, req *godo.DropletCreateRequest, timeout time.Duration) (int, error) {
	ctx := context.Background()
	droplet, resp, err := client.Droplets.Create(ctx, req)
	if err != nil {
		return -1, fmt.Errorf("error occured while creating droplet: code=%d, err=%s", resp.StatusCode, err)
	}

	var newDroplet *godo.Droplet
	tick := time.NewTicker(10 * time.Second)
	timer := time.NewTimer(timeout)
	defer tick.Stop()
	defer timer.Stop()
LOOP:
	for {
		select {
		case <-tick.C:
			log.Printf("Checking status of droplet: name=%s,id=%d, status=%s", droplet.Name, droplet.ID, droplet.Status)
			newDroplet, _, err = client.Droplets.Get(ctx, droplet.ID)
			if err != nil {
				err = fmt.Errorf("error getting droplet status (id=%d) to become active", droplet.ID)
				return -1, err
			}
			if newDroplet.Status == "active" {
				log.Printf("droplet changed status to: %s", newDroplet.Status)
				break LOOP
			}
		case <-timer.C:
			if _, er := client.Droplets.Delete(ctx, droplet.ID); er != nil {
				log.Printf("Error while deleting staled droplet: name=%s, id=%d, status=%s, err=%s", droplet.Name, droplet.ID, droplet.Status, er)
			}
		default:
			log.Printf("Waiting for the droplet: name=%s,id=%d, status=%s", droplet.Name, droplet.ID, droplet.Status)
			time.Sleep(10 * time.Second)
		}
	}
	return droplet.ID, nil
}

func DeleteDroplets(droplets []string, client *godo.Client) ([]string, error) {
	var deleted []string
	undeleted := deleted
	var err error
	for _, droplet := range droplets {
		ID, _ := strconv.Atoi(droplet)
		if _, err := client.Droplets.Delete(context.Background(), ID); err == nil {
			deleted = append(deleted, droplet)
		} else { // do not exit try to delete each droplet
			log.Printf("error deleting droplet %s: %s", droplet, err)
			undeleted = append(undeleted, droplet)
		}
	}
	if len(undeleted) > 0 {
		err = fmt.Errorf("some droplets couldn't be destroyed: %v", undeleted)
	}
	return deleted, err
}

//func GetDropletByName(name string, tag string, client *godo.Client) (*godo.Droplet, error) {
//	allDroplets, err := ListDroplets(tag, client)
//	if err != nil {
//		return nil, err
//	}
//	for _, droplet := range allDroplets {
//		if droplet.Name == name {
//			return nil, err
//		}
//	}
//	return nil, fmt.Errorf("droplet %s not found", name)
//}
