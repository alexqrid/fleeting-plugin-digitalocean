# Fleeting plugin for DigitalOcean (DO)

This is a [fleeting](https://gitlab.com/gitlab-org/fleeting/fleeting) plugin for the DigitalOcean.

[![Pipeline Status](https://gitlab.com/alexqrid/fleeting-plugin-digitalocean/badges/main/pipeline.svg)](https://gitlab.com/alexqrid/fleeting-plugin-digitalocean/commits/main)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/alexqrid/fleeting-plugin-digitalocean)](https://goreportcard.com/report/gitlab.com/alexqrid/fleeting-plugin-digitalocean)

## Build the plugin

To generate the binary:

1. Ensure `$GOPATH/bin` is on your PATH.
1. Run:

```shell
cd cmd/fleeting-plugin-digitalocean/
go install 

If you manage go versions with asdf, run this command after you generate the binary:

```shell
asdf reshim
```

## Plugin configuration

The following parameters are supported:

| Parameter               | Type   | Description                                                                                                            |
|-------------------------|--------|------------------------------------------------------------------------------------------------------------------------|
| `access_token`          | string | DigitalOcean API Access Token.                                                                                         |
| `instance_slug`         | string | DigitalOcean instance slug (e.g. `s-4vcpu-8gb`)                                                                        |
| `image`                 | string | Instance image (e.g. `docker-20-04`)                                                                                   |
| `creation_wait_timeout` | string | Timeout of waiting for the creation of new droplet,  (e.g. "90s", "1.5h" or "2h45m")                                   |
| `cloud_init_filepath`   | string | Path to cloud init script, `docker-20-04` image has ufw enabled by default, may be disabled via this cloud init config |
| `name`                  | string | Name of the instance group. Used in ID generation                                                                      |
| `region_slug`           | string | DigitalOcean API Access Token.                                                                                         |
| `tag`                   | string | Tag that will be assigned to all created instances                                                                     |
| `ssh_private_key_file`  | string | Path to ssh private key file. </br>If it doesn't exist, ssh key will be generated and saved in this file.              |

### Default connector config

| Parameter                | Default |
|--------------------------|---------|
| `os`                     | `linux` |
| `username`               | `root`  |
| `protocol`               | `ssh`   |
| `use_static_credentials` | `false` |

For other instances, if `use_static_credentials` is false and the protocol is `ssh`, a new key pair is generated, added to the DigitalOcean account and attached to the instance .

## Required Permissions

A service account with the following roles and permissions is required:

Roles:

- `iam.serviceAccountUser`

Permissions:

- `compute.instanceGroupManagers.get`
- `compute.instanceGroupManagers.update`
- `compute.instances.get`
- `compute.instances.setMetadata`

## Examples
```toml
[runners.autoscaler.plugin_config]
access_token = "dop_v1_xxx"
image = "docker-20-04"
instance_slug = "s-4vcpu-8gb"
name = "test-runners"
region_slug = "fra1"
ssh_private_key_file = "/home/runner/cache/id_rsa"
tag = "ci-runners"
```

### GitLab Runner

For examples on using this plugin, see the [Instance executor](https://docs.gitlab.com/runner/executors/instance.html#examples) and [Docker Autoscaler executor](https://docs.gitlab.com/runner/executors/docker_autoscaler.html#examples) documentation.
