# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.0] - 2024-04-03
- Added droplet creation wait timeout (`creation_wait_timeout`)
- Added `cloud_init_filepath` parameter to provide cloud init script for a new droplets
- Fixed `Increase` function, previously it was creating a single droplet
- Fixed `ListDroplets` method, previously it returned empty slice of droplets of certain length that was wrongly interpreted in `Update` function
## [0.1.0] - 2024-03-29
- Initialized DigitalOcean plugin
