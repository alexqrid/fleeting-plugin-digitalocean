package digitalocean

import (
	"context"
	"fmt"
	"github.com/digitalocean/godo"
	"github.com/hashicorp/go-hclog"
	"gitlab.com/alexqrid/fleeting-plugin-digitalocean/internal/doclient"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
	"math/rand"
	"os"
	"path"
	"strconv"
	"sync"
	"time"
)

var _ provider.InstanceGroup = (*InstanceGroup)(nil)

var charset = []byte("abcdefghijklmnopqrstuvwxyz")

func randomString(n int) string {
	b := make([]byte, n)
	for i := range b {
		// randomly select 1 character from given charset
		b[i] = charset[rand.Intn(len(charset))]
	}
	return string(b)
}

type InstanceGroup struct {
	AccessToken         string `json:"access_token"`
	Name                string `json:"name"`
	RegionSlug          string `json:"region_slug"`
	Image               string `json:"image"`
	InstanceSlug        string `json:"instance_slug"`
	Tag                 string `json:"tag"` // runner instance tag
	SshPrivateKeyFile   string `json:"ssh_private_key_file"`
	CreationWaitTimeout string `json:"creation_wait_timeout"`
	CloudInitFilePath   string `json:"cloud_init_filepath"`

	userData string
	sshKeyID int // will be retrieved or created in DO
	size     int64
	client   *godo.Client
	settings provider.Settings
	logger   hclog.Logger
}

func (g *InstanceGroup) Init(ctx context.Context, logger hclog.Logger, settings provider.Settings) (provider.ProviderInfo, error) {
	var err error
	g.logger = logger.With("RegionSlug", g.RegionSlug, "image", g.Image, "InstanceSlug", g.InstanceSlug, "Account", g.Name)
	if g.AccessToken == "" {
		return provider.ProviderInfo{}, fmt.Errorf("access token for DO account not provided")
	}
	if _, err = time.ParseDuration(g.CreationWaitTimeout); err != nil {
		g.logger.Warn("error parsing duration from creation_wait_timeout", "creation_wait_timeout", g.CreationWaitTimeout, "err", err)
		g.CreationWaitTimeout = "5m"
		g.logger.Info("set default value 5m for creation_wait_timeout")
	}

	if userData, err := os.ReadFile(g.CloudInitFilePath); err != nil {
		g.logger.Warn("Error reading cloud init file", "err", err)
	} else {
		g.userData = string(userData)
	}

	if g.client, err = doclient.NewClient(g.AccessToken); err != nil {
		return provider.ProviderInfo{}, err
	}
	g.logger.Info("Successfully got account info from DO")
	g.settings = settings

	pemBytes, err := os.ReadFile(g.SshPrivateKeyFile)
	if err != nil {
		g.logger.Warn("couldn't read ssh private key", err)
	}

	g.settings.Key = pemBytes
	g.size = 0

	info := provider.ConnectInfo{ConnectorConfig: g.settings.ConnectorConfig}

	err = g.ssh(&info)
	if err != nil {
		return provider.ProviderInfo{}, err
	}

	return provider.ProviderInfo{
		ID:        path.Join("do", g.Name, g.RegionSlug),
		MaxSize:   100,
		Version:   Version.String(),
		BuildInfo: Version.BuildInfo(),
	}, nil

}

func (g *InstanceGroup) Update(ctx context.Context, update func(instance string, state provider.State)) error {
	var state provider.State
	droplets, err := doclient.ListDroplets(g.Tag, g.client)
	if err != nil {
		g.logger.Error("Error listing droplets", "err", err)
		return err
	}

	// https://docs.digitalocean.com/reference/api/api-try-it-now/#/Droplets/droplets_get
	for _, droplet := range droplets {
		switch droplet.Status {
		case "new":
			state = provider.StateCreating
		case "archive", "off":
			state = provider.StateDeleted
		case "active":
			state = provider.StateRunning
		default:
			state = provider.StateDeleted
		}
		update(fmt.Sprintf("%d", droplet.ID), state)
	}

	return nil
}

func (g *InstanceGroup) Increase(ctx context.Context, delta int) (succeeded int, err error) {
	var name string
	wg := &sync.WaitGroup{}
	errorCh := make(chan error, delta)
	waitTimeout, _ := time.ParseDuration(g.CreationWaitTimeout)
	g.logger.Info("Increasing pool size by ", "count", delta, "current_size", g.size)
	for i := 0; i < delta; i++ {
		name = fmt.Sprintf("ci-runner-%s-%d", randomString(4), time.Now().Unix())
		wg.Add(1)

		go func(name string, wg *sync.WaitGroup, errorCh chan error, timeout time.Duration) {
			defer wg.Done()
			// here can be a race due to referring g struct without locks,
			// but actually haven't caught it
			_, err := doclient.CreateDroplet(g.client, &godo.DropletCreateRequest{
				Name:   name,
				Region: g.RegionSlug,
				Size:   g.InstanceSlug,
				Image:  godo.DropletCreateImage{Slug: g.Image},
				SSHKeys: []godo.DropletCreateSSHKey{
					{
						ID: g.sshKeyID,
					},
				},
				Tags:     []string{g.Tag},
				UserData: g.userData,
			}, timeout)
			if err != nil {
				errorCh <- err
			}
		}(name, wg, errorCh, waitTimeout)
	}
	wg.Wait()
	close(errorCh)
	for err := range errorCh {
		g.logger.Error("Error while creating droplet", "err", err)
		delta--
	}
	g.size = g.size + int64(delta)
	g.logger.Info("Increased pool size by ", "count", delta, "current_size", g.size)
	return delta, nil
}

func (g *InstanceGroup) Decrease(ctx context.Context, instances []string) (succeeded []string, err error) {
	g.logger.Info("Decreasing instances by ", "count", len(instances), "current_pool_size", g.size)
	deleted, _ := doclient.DeleteDroplets(instances, g.client)
	g.logger.Info("Deleted instances with the following IDs: ", "ids", deleted)
	g.size = g.size - int64(len(instances))
	if g.size < 0 {
		g.size = 0
	}
	g.logger.Info("Decreased instances by ", "count", len(instances), "new_pool_size", g.size)
	return instances, err
}

func (g *InstanceGroup) ConnectInfo(ctx context.Context, instanceId string) (provider.ConnectInfo, error) {
	info := provider.ConnectInfo{ConnectorConfig: g.settings.ConnectorConfig}
	g.logger.Info("Requesting connection to droplet", "id", instanceId)
	dropletID, _ := strconv.Atoi(instanceId)
	droplet, _, err := g.client.Droplets.Get(ctx, dropletID)
	if err != nil || droplet.Status != "active" {
		g.logger.Error("Failed to get droplet", "dropletID", droplet.ID)
		return provider.ConnectInfo{}, err
	}

	if info.ExternalAddr, err = droplet.PublicIPv4(); err != nil {
		g.logger.Error("Failed to obtain public IPV4", "dropletID", droplet.ID)
		return provider.ConnectInfo{}, err
	}

	if info.InternalAddr, err = droplet.PrivateIPv4(); err != nil {
		g.logger.Error("Failed to obtain private IPV4", "dropletID", droplet.ID)
		return provider.ConnectInfo{}, err
	}

	if info.Username == "" {
		// DigitalOcean always creates instances with "root" username.
		info.Username = "root"
	}
	if info.Arch == "" {
		info.Arch = "amd64"
	}
	if info.OS == "" {
		info.OS = "linux"
	}
	if info.Protocol == "" {
		info.Protocol = provider.ProtocolSSH
	}
	return info, nil
}

// Shutdown as digitalocean charges for the shutdown instances
// let's make shutdown equivalent to delete
func (g *InstanceGroup) Shutdown(ctx context.Context) error {
	resp, err := g.client.Droplets.DeleteByTag(ctx, g.Tag)
	if err != nil {
		return fmt.Errorf("unable to delete all runners: code=%d err=%s", resp.StatusCode, err)
	}
	resp, err = g.client.Keys.DeleteByID(ctx, g.sshKeyID)
	if err != nil {
		return fmt.Errorf("unable to delete key: code=%d err=%s", resp.StatusCode, err)
	}
	return nil
}
