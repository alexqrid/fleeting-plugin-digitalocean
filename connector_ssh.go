package digitalocean

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"gitlab.com/alexqrid/fleeting-plugin-digitalocean/internal/doclient"
	"gitlab.com/gitlab-org/fleeting/fleeting/provider"
	"golang.org/x/crypto/ssh"
	"log"
	"os"
	"path/filepath"
)

type PrivPub interface {
	crypto.PrivateKey
	Public() crypto.PublicKey
}

// ssh creates new ssh key or parses existing one
func (g *InstanceGroup) ssh(info *provider.ConnectInfo) error {
	var key PrivPub
	var err error

	if info.Key != nil {
		priv, err := ssh.ParseRawPrivateKey(info.Key)
		if err != nil {
			return fmt.Errorf("reading private key: %w", err)
		}
		var ok bool
		key, ok = priv.(PrivPub)
		if !ok {
			return fmt.Errorf("key doesn't export PublicKey()")
		}
	} else {
		log.Println("Generate new private key")
		key, err = rsa.GenerateKey(rand.Reader, 4096)
		if err != nil {
			return fmt.Errorf("generating private key: %w", err)
		}
		privDER := x509.MarshalPKCS1PrivateKey(key.(*rsa.PrivateKey))
		privPEM := pem.Block{
			Type:  "RSA PRIVATE KEY",
			Bytes: privDER,
		}

		g.settings.Key = pem.EncodeToMemory(&privPEM)
		if err := os.MkdirAll(filepath.Dir(g.SshPrivateKeyFile), 0700); err != nil {
			log.Printf("couldn't create path %s. Won't save generated private key.", filepath.Dir(g.SshPrivateKeyFile))
		} else {
			if err := os.WriteFile(g.SshPrivateKeyFile, g.settings.Key, 0600); err != nil {
				log.Printf("couldn't save generated private key in %s, err=%s", g.SshPrivateKeyFile, err)
			}
		}

	}
	sshPubKey, err := ssh.NewPublicKey(key.Public())
	if err != nil {
		return fmt.Errorf("generating ssh public key: %w", err)
	}
	sshKey := string(ssh.MarshalAuthorizedKey(sshPubKey))[:len(ssh.MarshalAuthorizedKey(sshPubKey))-1]
	// create or retrieve
	if g.sshKeyID, err = doclient.CreateOrGetSSHKey(sshKey, g.client); err != nil {
		return err
	}
	return nil
}
